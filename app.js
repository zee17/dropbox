var express = require('express');
var http = require('http');
var path = require('path');
var bodyParser = require('body-parser');
var favicon = require('serve-favicon');
var expressValidator = require('express-validator');
var cookieParser = require('cookie-parser');
var upload = require('express-fileupload');
var routes = require('./routes');
var user = require('./routes/user');
var home = require('./routes/home');
var session = require('client-sessions');
var fs = require("fs");
var ejs = require("ejs");
var nodemailer = require('nodemailer');
	
var app = express();
app.use(upload());
//all environments
//configure the sessions with our application
app.use(session({   
	cookieName: 'session',    
	secret: 'cmpe273_dropbox_project',    
	duration: 30 * 60 * 30000,    //setting the time for active session
	activeDuration: 5 * 60 * 1000,  })); // setting time for the session to be active when the window is open // 5 minutes set currently
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(expressValidator());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.cookieParser());
app.use(session({secret: 'zeeshanstring', saveUninitialized: true, resave: true}));


// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

//All Get and Post 
app.get('/signup', home.signup);
app.post('/afterSignUp', home.afterSignUp);
app.get('/signin', home.signin);
app.get('/afterSignIn', home.afterSignIn);
app.post('/setSession', home.setSession);
app.get('/getAllUsers', home.getAllUsers);
app.get('/uploads', home.uploads);
app

app.post('/uploads', function(req,res){
	console.log("upload");
	console.log(req.files);
	//if file exists
	if(req.files.filenames){
		//extracting file details
		var file = req.files.filenames,
			filename = file.name;
			//location of upload
			let des = path.join(__dirname,"./Uploads",filename);
			console.log("Uploaded at : " + des);
			file.mv(des, function(err){
				//if error
				if(err){
					console.log(err);
					res.send("error occured");
				}
				else{
					//re-render homepage with all details
					var directoryName = "Uploads";
					var dirBuffer = Buffer.from(directoryName);
					var allFiles = fs.readdirSync(directoryName);
					console.log("Files are:");
					console.log(allFiles);
					ejs.renderFile('./views/homepage.ejs', {data : allFiles}, function(err, result) {
			        // render on success
			        if (!err) {
			            res.end(result);
			        }
			        // render or error
			        else {
			            res.end('An error occurred');
			            console.log(err);
			        }
			    });
				}
			});

		}
}); 
//create directory 
app.post('/createDirectory', function(req,res){
	//extracting information from html
	var dirName = req.body.directory;
	console.log(dirName);
	//path for creation
	fs.mkdirSync('./Uploads/' + dirName);
	console.log("New directory created!")
				//re-rendering homepage with all details
				var directoryName = "Uploads";
				var dirBuffer = Buffer.from(directoryName);
				var allFiles = fs.readdirSync(directoryName);			
				ejs.renderFile('./views/homepage.ejs', {data : allFiles}, function(err, result) {
			        // render on success
			        if (!err) {
			            res.end(result);
			        }
			        // render or error
			        else {
			            res.end('An error occurred');
			            console.log(err);
			        }
			    });		
});
app.post('/share', function(req,res){
				console.log(req.body);
				var recipient = req.body.sharemail;
				//setting up transport service
				var mailSetup = nodemailer.createTransport({
				 service: 'gmail',
				 auth: {
				        user: 'zeeshancmpe273@gmail.com',
				        pass: 'zeeshandropbox'
    					}
				});
				//setting up auth object
				const mailInfo = {
				  from: 'zeeshancmpe273@gmail.com', // Sender 
				  to: recipient, // Receiver(s)
				  subject: 'Claim Dropbox file!', // Subject line
				  html: '<p>Hi. Zeeshan shared a Dropbox file with you. Please login or signup with Dropbox at http://localhost:3000/ </p>'// text body
				};
					//sending email
				mailSetup.sendMail(mailInfo, function (err, info) {
				   if(err)
				     console.log(err)
				   else
				   {
				   	console.log("Email sent:");
				     console.log(info);
				   }
});
				//re-rendering homepage with all details
				var directoryName = "Uploads";
				var dirBuffer = Buffer.from(directoryName);
				var allFiles = fs.readdirSync(directoryName);			
				ejs.renderFile('./views/homepage.ejs', {data : allFiles}, function(err, result) {
			        // render on success
			        if (!err) {
			            res.end(result);
			        }
			        // render or error
			        else {
			            res.end('An error occurred');
			            console.log(err);
			        }
			    });		
});
app.get('/', home.dashboard);
app.post('/logout',home.logout);
app.get('/aboutMe', home.aboutMe);

//server
http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
