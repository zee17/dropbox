var ejs = require("ejs");
var mysql = require('./mysql');
var express = require('express');
var fs = require("fs");
var crypto = require("crypto");
var key = "zee123";



function dashboard(req,res) {

	ejs.renderFile('./views/dashboard.ejs',function(err, result) {
	   // render on success
	   if (!err) {
	            res.end(result);
	   }
	   // render or error
	   else {
	            res.end('An error occurred');
	            console.log(err);
	   }
   });
}

function signin(req,res) {

	ejs.renderFile('./views/signin.ejs',function(err, result) {
	   // render on success
	   if (!err) {
	            res.end(result);
	   }
	   // render or error
	   else {
	            res.end('An error occurred');
	            console.log(err);
	   }
   });
}

function signup(req,res) {

	ejs.renderFile('./views/signup.ejs',function(err, result) {
	   // render on success
	   if (!err) {
	            res.end(result);
	   }
	   // render or error
	   else {
	            res.end('An error occurred');
	            console.log(err);
	   }
   });
}


function afterSignUp(req,res)
{
			//extracting data from db
			var name1 =req.param('inputUsername');
			var email1 = req.param('emailaddress');
			var password1 = req.param('inputPassword');
			var passwordencrypt = crypto.createCipher("aes-256-ctr",key).update(password1,"utf-8", "hex");
			//validation
			if(name1 == '' || email1 == '' || password1== '')
			{
				res.render('failSignUp');
			}
			//fetching query from MySql
			var sql = "INSERT INTO users1 (name, username, password) VALUES ("+"'"+name1+"',"+"'"+email1+"',"+"'"+passwordencrypt +"')";
		    mysql.fetchData(function (err, results) {
		    if (err){
				console.log("Invalid signup");
				ejs.renderFile('./views/failSignUp.ejs',function(err, result) {
			        // render on success
			        if (!err) {
			            res.end(result);
			        }
			        // render or error
			        else {
			            res.end('An error occurred');
			            console.log(err);
			        }
			    });
			}
		    else{
		    	console.log("Record inserted");
				console.log("valid Signup");
				ejs.renderFile('./views/successSignUp.ejs', function(err, result) {
			        // render on success
			        if (!err) {
			            res.end(result);
			        }
			        // render or error
			        else {
			            res.end('An error occurred');
			            console.log(err);
			        }
			    });
		 }},sql);
			
		}  

function setSession(req,res){

// Generating query
	console.log("Inside set session in");
	var inputUsername = req.param("inputUsername");
	var inputPassword = req.param("inputPassword");
	console.log(inputPassword);
	var json_responses;
	var passworddecrypt = crypto.createCipher("aes-256-ctr",key).update(inputPassword,"utf-8", "hex");
	console.log(passworddecrypt);
	var getUser="select * from users1 where username='"+req.param("inputUsername")+"' and password='" + passworddecrypt +"'";
	console.log("Query is:"+getUser);
	//Getting data using above generated query
	mysql.fetchData(function(err,results){
		if(err){
			throw err;
		}
		else 
		{
			if(results.length > 0){
				//Assigning the session
				req.session.inputUsername = inputUsername;
				console.log("Session initialized");
				json_responses = {"statusCode" : 200,
									"message" : "Success. Please navigate to http://localhost:3000/afterSignIn "
									};
				res.send(json_responses);
				console.log("IMP : Please redirect to 'http://localhost:3000/afterSignIn' to reach homepage !");

			}
		
			else { 
				//Passing error code   
				json_responses = {"statusCode" : 401,
									"message" : "Invalid username or password. Please go back and try again "
									};
				res.send(json_responses);
				console.log("IMP : Incorrect username or password. Please go back to login page !");

			
			}
		}  
	},getUser);


	
}

function afterSignIn(req,res)
{

	console.log("Inside after sign in");
	
			//checking if session persists
			if(req.session.inputUsername){

				console.log("valid Login");
				res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
				console.log("Header set!");
				//reading contents of directory
				console.log("	Reading contents from directory!");
				var directoryName = "Uploads";
				var dirBuffer = Buffer.from(directoryName);
				var allFiles = fs.readdirSync(directoryName);
				console.log("Files are:");
				console.log(allFiles);
				ejs.renderFile('./views/homepage.ejs', {data : allFiles}, function(err, result) {
			        // render on success
			        if (!err) {
			            res.end(result);
			        }
			        // render or error
			        else {
			            res.end('An error occurred');
			            console.log(err);
			        }
			    });
				//res.render('./views/homepage.ejs',{inputUsername:req.session.inputUsername});
				
			}
		
			else {    
		
				console.log("Invalid Login");
				ejs.renderFile('./views/failLogin.ejs', function(err, result) {
			        // render on success
			        if (!err) {
			            res.end(result);
			        }
			        // render or error
			        else {
			            res.end('An error occurred');
			            console.log(err);
			        }
			    });
			}
	
}

function logout(req,res)
{
	//destroying session on logout
	req.session.destroy();
	console.log("Session destroyed");
	res.redirect('/');
}


function getAllUsers(req,res)
{
	//getting details of user
	var getAllUsers = "select * from users";
	console.log("Query is:"+getAllUsers);
	
	mysql.fetchData(function(err,results){
		if(err){
			throw err;
		}
		else 
		{
			if(results.length > 0){
				
				var rows = results;
				var jsonString = JSON.stringify(results);
				var jsonParse = JSON.parse(jsonString);
				
				console.log("Results Type: "+(typeof results));
				console.log("Result Element Type:"+(typeof rows[0].emailid));
				console.log("Results Stringify Type:"+(typeof jsonString));
				console.log("Results Parse Type:"+(typeof jsString));
				
				console.log("Results: "+(results));
				console.log("Result Element:"+(rows[0].emailid));
				console.log("Results Stringify:"+(jsonString));
				console.log("Results Parse:"+(jsonParse));
				
				ejs.renderFile('./views/homepage.ejs',{data:jsonParse},function(err, result) {
			        // render on success
			        if (!err) {
			            res.end(result);
			        }
			        // render or error
			        else {
			            res.end('An error occurred');
			            console.log(err);
			        }
			    });
			}
			else {    
				
				console.log("No users found in database");
				ejs.renderFile('./views/failLogin.ejs',function(err, result) {
			        // render on success
			        if (!err) {
			            res.end(result);
			        }
			        // render or error
			        else {
			            res.end('An error occurred');
			            console.log(err);
			        }
			    });
			}
		}  
	},getAllUsers);
}
function uploads(req,res) {

	ejs.renderFile('./views/homepage.ejs',function(err, result) {
		// render on success
		if (!err)
		 {
			  res.end(result);
		 }
	    // render or error
	    else
	     {
			res.end('An error occurred');
			console.log(err);
		 }
	});
}
function aboutMe(req,res) {

		// check user already exists
	var getAllUsers = "select * from users1";
	console.log("Query is:"+getAllUsers);
	
	mysql.fetchData(function(err,results){
		if(err){
			throw err;
		}
		else 
		{
			if(results.length > 0){
				
				var rows = results;
				var jsonString = JSON.stringify(results);
				var jsonParse = JSON.parse(jsonString);
				
				console.log("Results Type: "+(typeof results));
				console.log("Result Element Type:"+(typeof rows[0].emailid));
				console.log("Results Stringify Type:"+(typeof jsonString));
				console.log("Results Parse Type:"+(typeof jsString));
				
				console.log("Results: "+(results));
				console.log("Result Element:"+(rows[0].emailid));
				console.log("Results Stringify:"+(jsonString));
				console.log("Results Parse:"+(jsonParse));
				
				ejs.renderFile('./views/aboutMe.ejs',{data:jsonParse},function(err, result) {
			        // render on success
			        if (!err) {
			            res.end(result);
			        }
			        // render or error
			        else {
			            res.end('An error occurred');
			            console.log(err);
			        }
			    });
			}
			else {    
				
				console.log("No users found in database");
				ejs.renderFile('./views/failLogin.ejs',function(err, result) {
			        // render on success
			        if (!err) {
			            res.end(result);
			        }
			        // render or error
			        else {
			            res.end('An error occurred');
			            console.log(err);
			        }
			    });
			}
		}  
	},getAllUsers);
}


exports.signin=signin;
exports.signup=signup;
exports.afterSignIn=afterSignIn;
exports.afterSignUp=afterSignUp;
exports.getAllUsers=getAllUsers;
exports.dashboard=dashboard;
exports.uploads=uploads;
exports.setSession=setSession;
exports.logout=logout;
exports.aboutMe=aboutMe;
