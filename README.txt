

# NodeExpress



## Usage



## Developing



### Tools

Created with [Nodeclipse](https://github.com/Nodeclipse/nodeclipse-1)
 ([Eclipse Marketplace](http://marketplace.eclipse.org/content/nodeclipse), [site](http://www.nodeclipse.org))   

Nodeclipse is free open-source project that grows with your contributions.

Steps to run:

In cmd,

1. navigate to application folder and install all dependencies using npm install
2. Run  node app.js.
3.Port used for sever = 3000
3. In the browser, connect to http://localhost:3000/

Note: I have included the db file as 'users_users1' in the application folder.